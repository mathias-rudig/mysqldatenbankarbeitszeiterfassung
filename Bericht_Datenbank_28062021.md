# Semesterprojekt Datenbanken

## Einleitung
Dieses Abschlussprojekt beschäftigt sich mit dem Thema Datenbankmodellierung und Implementierung eines Zeiterfassungssystems für Mitarbeiter. Ziel ist es alle Schritte der Datenmodellierung möglichst praxisgerecht umzusetzen. Von der Anforderungsanalyse über das Entwerfen des Datenmodells, dem Mapping in ein relationales Modell bis hin zur Implementierung werden alle Ergebnisse in diesem Dokument zusammengefasst und beschrieben. Die Implementierung wird mittels Docker-Containern innerhalb einer virtuellen Umgebung, unter Verwendung von mysql 8.0.23 und adminer 4.8.1 umgesetzt.

<br>

# Inhaltsverzeichnis
- [Semesterprojekt Datenbanken](#semesterprojekt-datenbanken)
  - [Einleitung](#einleitung)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Verwendete Technologien](#verwendete-technologien)
- [Aufgabenstellung](#aufgabenstellung)
- [Vorbereitung](#vorbereitung)
  - [Vorbereitung des Servers](#vorbereitung-des-servers)
  - [Vorbereitung ssh-Remote Verbindung](#vorbereitung-ssh-remote-verbindung)
  - [Vorbereitung Datenbank mittels Docker-Containern](#vorbereitung-datenbank-mittels-docker-containern)
- [Durchführung](#durchführung)
- [Entity-Relationship Diagramm](#entity-relationship-diagramm)
- [Relationales Datenbankmodell](#relationales-datenbankmodell)
- [Physisches Datenbankmodell](#physisches-datenbankmodell)
  - [Statements zur Datenbank Erstellung](#statements-zur-datenbank-erstellung)
  - [Aufbau der Datenbank](#aufbau-der-datenbank)
  - [Statements für zentrale Interaktionen mit Datenbank](#statements-für-zentrale-interaktionen-mit-datenbank)
    - [INSERT-Statements](#insert-statements)
    - [SELECT-Statements](#select-statements)
    - [Ausgabe SELECT-Statement](#ausgabe-select-statement)
- [Diskussion](#diskussion)
- [Literaturverzeichnis](#literaturverzeichnis)
  - [Author](#author)

<br>

# Verwendete Technologien
* Oracle Virtual Box version 6.1.16 r140961
* Linux Server version Ubuntu 20.04.1 LTS
* Visual Studio Code Version: 1.54.3
* ssh 0.65.1 (Plug-in Visual Studio Code)
* bash Version GNU bash, version 5.0.17(1)
* Docker 19.03.13
    * Verwendung einer bereitgestellten docker-compose.yaml (Stolz Stefan)
    * adminer 4.8.0
    * mysql 8.0.23
* Java Entwicklungsumgebung IntelliJ IDEA 2021.1.1 (Ultimate Edition)
  * java openjdk 16.0.1
  * MySQL Connector java  8.0.25

<br>

# Aufgabenstellung
Das Zeiterfassungssystem ist für Mitarbeiter ausgelegt und enthält Einträge von Arbeitszeiten und Fehlzeiten, welche jeweils einen bestimmten Grund zugewiesen bekommen. Zudem bekommt jeder Mitarbeiter eine Sollarbeitszeit und Anzahl an Urlaubstagen zugewiesen, um den aktuellen Stand der geleisteten Arbeit oder Fehlzeit überprüfen respektive berechnen zu können.

<br>

# Vorbereitung
## Vorbereitung des Servers
In meinem Fall installiere ich einen Ubuntu Server in einer virtuellen Umgebung (Vitual-Box). Bei der Installation sollte darauf geachtet werden, dass man den open-ssh Server und Docker gleich mitinstalliert, ansonsten können diese auf bestehenden Installationen mit `apt install docker` und `apt install openssh-server` nachinstalliert werden. Es sollte ein Benutzer angelegt werden, mit dem später mittels ssh ein Zugriff auf den Server erfolgt. Zudem sollte eine Netzwerkverbindung zwischen dem Host(lokalem Rechner) und dem virtuellen Server existieren, welche per `ping` geprüft werden kann. Mit `sudo systemctl status ssh` kann noch überprüft werden, ob der ssh-Dienst läuft.

<br>

## Vorbereitung ssh-Remote Verbindung
Damit ein einfaches Handling und ein vereinfachter Zugriff per Visual Studio Code mittels ssh aufgebaut werden kann, muss das in den verwendeten Technologien angeführte Plug-in "Remote - SSH 0.65.1" installiert und mit der richtigen Server-IP, Benutzername und Passwort konfiguriert werden. Somit wird ein direkter ssh-Zugriff ermöglicht und das Arbeiten und Verwalten von/mit Files sehr einfach gestaltet.

<br>

## Vorbereitung Datenbank mittels Docker-Containern
Die bereitgestellte `docker-compose.yaml` wird in einem beliebigen Verzeichnis abgelegt und aus diesem können dann mit `docker-compose up -d` die Container im Hintergrund gestartet werden. Mit dem Befehl `docker ps` hat man eine Einsicht auf die aktuell laufenden respektive gestoppten Container. Nun hat man Zugriff auf die Datenbank (MySql) und die Verwaltungssoftware (Adminer). Die Zugangsdaten und Ports können aus der `docker-compose.yaml` entnommen werden um mittels IP-Adresse und Port auf die Grafische Oberfläche zugreifen zu können. Somit wären alle Vorbereitungen abgeschlossen und die Datenbank kann aufgesetzt respektive mit Datensätzen befüllt werden. 

<br>

# Durchführung

Schritte:
1. Datenbank modelieren
2. Relationales Datenbankschema erstellen
3. Implementieren der Datenbank in einer SQL-Datei
4. Datenbank anlegen (Adminer) und SQL-Datei importieren
5. Anwenden der SELECT-Statements
 
Sofern die Datenbank am Adminer angelegt wurde, kann die SQL-Datei, welche Tabellen erstellt und Datensätze einfügt, implementiert werden. Dafür muss die Datei auf unserem Server in einem beliebigen Verzeichnis abgelegt werden, um diese anschließend mit `docker exec -i mysql mysql -t -uroot -p123 < ./speicherort/datei.sql` in unsere Datenbank zu schreiben. Bei fehlerfreier Ausführung kann nun eine zusätzliche SQL-Datei mit den SELECT-Statements auf dem selben Weg ausgeführt und die selektierten Tabellen auf der CLI ausgegeben werden. Über diesen Weg kann mit der gesammten MySql-Datenbank interagiert werden.Die verwendeteten SQL-Dateien folgen in untenstehenden Punkten.

<br>

Zusätzlich wurde noch ein Java-CLI-Programm angefügt, welches eine Verbindung mittels Socket (ip-adresse:port) zur MySql-Datenbank aufbaut und hinterlegte SELECT-Statemens bereitstellt.

<br>

# Entity-Relationship Diagramm
![ERD-Diagramm](./bilder/aufbauDatenbank.png)

<br>

# Relationales Datenbankmodell

MITARBEITER
| key                  | optionality          | column name        |
| -------------------- | -------------------- | ------------------ |
| pk                   | *                    | mitarbeiter_id     |
|                      | *                    | vorname            |
|                      | *                    | nachname           |
|                      | *                    | geburtsdatum       |
|                      | *                    | einstelldatum      |
| fk1                  | *                    | adresse_id         |
| fk2                  | *                    | sollarbeitszeit_id |
| fk3                  | *                    | urlaub_id          |

ADRESSE
| key                  | optionality          | column name        |
| -------------------- | -------------------- | ------------------ |
| pk                   | *                    | adresse_id         |
|                      | *                    | strasse            |
|                      | *                    | hausnummer         |
|                      | *                    | plz                |
|                      | *                    | ort                |

SOLLARBEITSZEIT
| key                  | optionality          | column name        |
| -------------------- | -------------------- | ------------------ |
| pk                   | *                    | sollarbeitszeit_id |
|                      | *                    | wochenstunden      |
|                      | *                    | stunden_pro_tag    |

URLAUB
| key                  | optionality          | column name              |
| -------------------- | -------------------- | ------------------------ |
| pk                   | *                    | urlaub_id                |
|                      | *                    | urlaubsanspruch_pro_jahr |

ANWESENHEIT
| key                  | optionality          | column name        |
| -------------------- | -------------------- | ------------------ |
| pk                   | *                    | anwesenheit_id     |
|                      | *                    | datum              |
|                      | *                    | arbeitszeit_von    |
|                      | *                    | arbeitszeit_bis    |
|                      | o                    | anwesenheits_art   |
| fk                   | *                    | mitarbeiter_id     |

FEHLZEIT
| key                  | optionality          | column name        |
| -------------------- | -------------------- | ------------------ |
| pk                   | *                    | fehlzeit_id        |
|                      | *                    | datum              |
|                      | *                    | fehlzeit_von       |
|                      | *                    | fehlzeit_bis       |
|                      | *                    | grund              |
|                      | *                    | entschuldigt       |
| fk                   | *                    | mitarbeiter_id     |

<br>

# Physisches Datenbankmodell
## Statements zur Datenbank Erstellung
```sql
-- verwenden einer bereits erstellten Datenbank
use zeiterfassung;

-- Wichtig, die Reihenfolge muss beim Löschen und Erstellen von Tabellen beachtet werden (Foreign-Key Constraint)

-- Löschen der Tabellen falls vorhanden
DROP TABLE IF EXISTS fehlzeit;
DROP TABLE IF EXISTS anwesenheit;
DROP TABLE IF EXISTS mitarbeiter;
DROP TABLE IF EXISTS adresse;
DROP TABLE IF EXISTS sollarbeitszeit;
DROP TABLE IF EXISTS urlaub;


-- Erstellen der Tabellen
CREATE TABLE IF NOT EXISTS adresse (
    adresse_id int NOT NULL auto_increment PRIMARY KEY,
    strasse varchar(255) NOT NULL,
    hausnummer varchar(255) NOT NULL,
    plz char(4) NOT NULL,
    ort varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS sollarbeitszeit (
    sollarbeitszeit_id int NOT NULL auto_increment PRIMARY KEY,
    wochenstunden float NOT NULL
    stunden_pro_tag float NOT NULL
);

CREATE TABLE IF NOT EXISTS urlaub (
    urlaub_id int NOT NULL auto_increment PRIMARY KEY,
    urlaubsanspruch_pro_jahr float NOT NULL
);

CREATE TABLE IF NOT EXISTS mitarbeiter (
    mitarbeiter_id int NOT NULL auto_increment PRIMARY KEY,
    vorname varchar(255) NOT NULL,
    nachname varchar(255) NOT NULL,
    geburtsdatum date NOT NULL,
    einstelldatum date NOT NULL,
    adresse_id int NOT NULL,
    sollarbeitszeit_id int NOT NULL,
    urlaub_id int NOT NULL,
    foreign key(adresse_id) references adresse (adresse_id),
    foreign key(sollarbeitszeit_id) references sollarbeitszeit (sollarbeitszeit_id),
    foreign key(urlaub_id) references urlaub (urlaub_id)
);

CREATE TABLE IF NOT EXISTS anwesenheit (
    anwesenheit_id int NOT NULL auto_increment PRIMARY KEY,
    datum date NOT NULL,
    arbeitszeit_von time NOT NULL,
    arbeitszeit_bis time NOT NULL,
    anwesenheits_art varchar(255) NULL,
    mitarbeiter_id int NOT NULL,
    foreign key(mitarbeiter_id) references mitarbeiter (mitarbeiter_id)
);

CREATE TABLE IF NOT EXISTS fehlzeit (
    fehlzeit_id int NOT NULL auto_increment PRIMARY KEY,
    datum date NOT NULL,
    fehlzeit_von time NOT NULL,
    fehlzeit_bis time NOT NULL,
    grund varchar(255) NOT NULL,
    entschuldigt boolean NOT NULL,
    mitarbeiter_id int NOT NULL,
    foreign key(mitarbeiter_id) references mitarbeiter (mitarbeiter_id)
);
```
<br>

## Aufbau der Datenbank
![ERD-Diagramm](./bilder/erDiagramm.png)

<br>

## Statements für zentrale Interaktionen mit Datenbank
### INSERT-Statements
```sql

-- ADRESSE
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (1, 'Schnann', '4', '6574', 'Schnann');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (2, 'Prantauersiedlung', '21', '6500', 'Landeck');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (3, 'Via-Claudia-Augusta', '29', '6533', 'Fiss');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (4, 'Eileweg', '6', '6522', 'Prutz');
INSERT INTO `adresse` (`adresse_id`, `strasse`, `hausnummer`, `plz`, `ort`) VALUES (5, 'Hauptstrasse', '94', '6511', 'Zams');

-- SOLLARBEITSZEIT
INSERT INTO `sollarbeitszeit` (`sollarbeitszeit_id`, `wochenstunden`, `stunden_pro_tag`) VALUES (1, 40.0, 8);
INSERT INTO `sollarbeitszeit` (`sollarbeitszeit_id`, `wochenstunden`, `stunden_pro_tag`) VALUES (2, 38.5, 7.7);

-- URLAUB
INSERT INTO `urlaub` (`urlaub_id`, `urlaubsanspruch_pro_jahr`) VALUES (1, 25.0);

-- MITARBEITER
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (1, 'Clemens', 'Kerber', '1999-06-22', '2021-01-01', 1, 2, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (2, 'Tobias', 'Tilg', '2002-02-20', '2020-01-01', 2, 1, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (3, 'Frederick', 'Moederndorfer', '1998-03-13', '2021-01-15', 3, 2, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (4, 'Dario', 'Skocibusic', '1993-06-23', '2020-05-01', 4, 1, 1);
INSERT INTO `mitarbeiter` (`mitarbeiter_id`, `vorname`, `nachname`, `geburtsdatum`, `einstelldatum`, `adresse_id`, `sollarbeitszeit_id`, `urlaub_id`) VALUES (5, 'Mathias', 'Rudig', '1993-01-29', '2020-05-01', 5, 1, 1);

-- ANWESENHEIT
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (1, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (2, '2021-05-25', '04:50:38', '06:07:48', 'Homeoffice', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (3, '2021-05-26', '18:05:17', '03:24:45', 'Homeoffice', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (4, '2021-05-27', '04:22:38', '08:32:59', 'Homeoffice', 1);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (5, '2021-05-28', '14:24:58', '21:43:01', 'Homeoffice', 1);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (6, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 2);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (7, '2021-05-25', '04:50:38', '06:07:48', 'Buero', 2);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (8, '2021-05-26', '18:05:17', '03:24:45', 'Buero', 2);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (9, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (10, '2021-05-25', '04:50:38', '06:07:48', 'Aussendienst', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (11, '2021-05-26', '18:05:17', '03:24:45', 'Aussendienst', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (12, '2021-05-27', '04:22:38', '08:32:59', 'Aussendienst', 3);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (13, '2021-05-28', '14:24:58', '21:43:01', 'Aussendienst', 3);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (14, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (15, '2021-05-25', '04:50:38', '06:07:48', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (16, '2021-05-26', '18:05:17', '03:24:45', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (17, '2021-05-27', '04:22:38', '08:32:59', 'Buero', 4);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (18, '2021-05-28', '14:24:58', '21:43:01', 'Homeoffice', 4);

INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (19, '2021-05-24', '04:24:42', '20:01:09', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (20, '2021-05-25', '04:50:38', '06:07:48', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (21, '2021-05-26', '18:05:17', '03:24:45', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (22, '2021-05-27', '04:22:38', '08:32:59', 'Buero', 5);
INSERT INTO `anwesenheit` (`anwesenheit_id`, `datum`, `arbeitszeit_von`, `arbeitszeit_bis`, `anwesenheits_art`, `mitarbeiter_id`) VALUES (23, '2021-05-28', '14:24:58', '21:43:01', 'Buero', 5);

-- FEHLZEIT
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (1, '2021-05-31', '07:30:00', '15:12:00', 'Urlaub', 1, 1);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (2, '2021-05-27', '07:30:00', '15:30:00', 'Krank', 0, 2);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (3, '2021-05-08', '07:30:00', '15:30:00', 'Krank', 1, 2);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (4, '2021-05-31', '07:30:00', '15:30:00', 'Urlaub', 1, 2);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (5, '2021-05-17', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (6, '2021-05-18', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (7, '2021-05-19', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (8, '2021-05-20', '07:30:00', '15:12:00', 'Krank', 0, 3);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (9, '2021-05-21', '07:30:00', '15:12:00', 'Krank', 0, 3);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (10, '2021-05-17', '07:30:00', '15:30:00', 'Krank', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (11, '2021-05-18', '07:30:00', '15:30:00', 'Urlaub', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (12, '2021-05-19', '07:30:00', '15:30:00', 'Urlaub', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (13, '2021-05-20', '07:30:00', '15:30:00', 'Urlaub', 1, 4);
INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (14, '2021-05-21', '07:30:00', '15:30:00', 'Urlaub', 1, 4);

INSERT INTO `fehlzeit` (`fehlzeit_id`, `datum`, `fehlzeit_von`, `fehlzeit_bis`, `grund`, `entschuldigt`, `mitarbeiter_id`) VALUES (15, '2021-05-31', '07:30:00', '15:12:00', 'Urlaub', 1, 4);

```

<br>

### SELECT-Statements
```sql
use zeiterfassung;

-- Das folgende SELECT-STATEMENT dient zur Ermittlung aller Mitarbeiter un deren Adresse,
-- zudem wird die Ausgabe anhand der mitarbeiter_id aufsteigend sortiert.
SELECT
       mitarbeiter.mitarbeiter_id AS 'ID',
       mitarbeiter.vorname AS 'Vorname',
       mitarbeiter.nachname AS 'Nachname',
       mitarbeiter.geburtsdatum AS 'Geburtsdatum',
       mitarbeiter.einstelldatum AS 'Einstelldatum',
       CONCAT(adresse.strasse, ' ', adresse.hausnummer) AS 'Strasse',
       adresse.plz AS 'PLZ',
       adresse.ort AS 'Ort'
FROM 
      mitarbeiter
INNER JOIN adresse USING(adresse_id)
ORDER BY mitarbeiter_id ASC;


SET @anwesenheits_art = 'Homeoffice';
-- SET @anwesenheits_art = 'Buero';
-- SET @anwesenheits_art = 'Montage';

-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Gesamt-Arbeitstage des aktuellen Jahres pro Mitarbeiter anhand einer bestimmten Art, 
-- somit kann der Anteil der jeweiligen Art ermittelt werden. Zudem wird die Ausgabe anhand der mitarbeiter_id aufsteigend sortiert.
Select  mitarbeiter.mitarbeiter_id AS 'ID', 
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        -- Berechnen der verbrauchten Urlaubstage innerhalb eines Jahres, gerundet auf zwei Nachkommastellen. 
        -- Für die Berechnung muss die Zeit zuerst in Sekunden umgewandelt werden um den Wert richtig ermitteln zu können, 
        -- zudem wird in einer "Case When-Bedingung" sichergestellt, dass nur positive Zahlen ermittelt werden.
        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 
        THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))
        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'TageHomeofficeAktuellesJahr'
From anwesenheit
    INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
    INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
-- Hier wird vom aktuellen Jahr der erste Tag zu ermittelt.
WHERE anwesenheit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      -- Hier wird vom aktuellen Jahr der ersteletzte Tag ermittelt.
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND anwesenheit.anwesenheits_art = @anwesenheits_art -- Variable von oben wird verwendet
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Arbeitszeitseinträge des Vormonats pro Mitarbeiter
-- anhand einer bestimmten Art, welche anhand der mitarbeiter_id und dem Datumsstempel aufsteigend sortiert werden.
Select  mitarbeiter.mitarbeiter_id AS 'ID', 
CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
anwesenheit.datum AS 'Datum',
ROUND(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))
        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END)/3600, 2) AS 'ArbeitszeitHomeoffice'
From anwesenheit
INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE anwesenheit.datum >= DATE_FORMAT(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH), '%Y-%m-01') 
      AND anwesenheit.datum <= LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH))
      AND anwesenheit.anwesenheits_art = @anwesenheits_art
ORDER BY mitarbeiter.mitarbeiter_id, anwesenheit.datum ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Gesamt-Arbeitszeit des Vormonats pro Mitarbeiter, 
-- welche anhand der mitarbeiter_id aufsteigend sortiert wird.
Select  mitarbeiter.mitarbeiter_id AS 'ID', CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(arbeitszeit_bis, arbeitszeit_von) < 0 THEN (SUBTIME(arbeitszeit_von, arbeitszeit_bis))
        ELSE SUBTIME(arbeitszeit_bis, arbeitszeit_von) END))/3600, 2) AS 'IstStundenVormonat',
        ROUND(DAYOFMONTH(LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH)))/7*sollarbeitszeit.wochenstunden, 2) AS 'SollStunden'
From anwesenheit
INNER JOIN mitarbeiter ON anwesenheit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE anwesenheit.datum >= DATE_FORMAT(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH), '%Y-%m-01') 
      AND anwesenheit.datum <= LAST_DAY(DATE_SUB(SYSDATE(),INTERVAL 1 MONTH))
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der Krankenstandtage des aktuellen Jahres pro Mitarbeiter,
-- welche anhand der mitarbeiter_id aufsteigend sortiert wird.
Select  mitarbeiter.mitarbeiter_id AS 'ID',
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 THEN (SUBTIME(fehlzeit_von, fehlzeit_bis))
        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) 
        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'TageKrankenstandAktuellesJahr' 
From fehlzeit
INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND fehlzeit.grund = 'Krank'
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der unentschuldigten Fehlzeiten des aktuellen Jahres pro Mitarbeiter,
-- welche anhand der mitarbeiter_id aufsteigend sortiert wird.
Select  mitarbeiter.mitarbeiter_id AS 'ID',
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        COUNT(fehlzeit.entschuldigt) AS 'UnentschuldigteFehlzeitenAktullesJahr' 
From fehlzeit
INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND fehlzeit.entschuldigt = 0
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;


-- Das folgende SELECT-STATEMENT dient zur Ermittlung der schon verbrauchten Urlaubstage und der noch verfügbaren anhand des
-- aktuellen Jahres unter Berücksichtigung der Gesamtanzahl an Urlaubstagen pro Jahr. Zudem wird die Ausgabe anhand der 
-- mitarbeiter_id aufsteigend sortiert.
Select  mitarbeiter.mitarbeiter_id AS 'ID',
        CONCAT(mitarbeiter.vorname, ' ',mitarbeiter.nachname) AS 'Name', 
        ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 
        THEN (SUBTIME(fehlzeit_von, fehlzeit_bis)) 
        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) 
        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'VerbrauchteUrlaubsTageAktuellesJahr',
        -- Selbes Scheme als in der voherigen Berechnung, nur dass der ermittelte Wert der Gesamtanzahl der Urlaubstage abgezogen wird.
        urlaub.urlaubsanspruch_pro_jahr - ROUND(SUM(TIME_TO_SEC(CASE WHEN SUBTIME(fehlzeit_bis, fehlzeit_von) < 0 
        THEN (SUBTIME(fehlzeit_von, fehlzeit_bis))
        ELSE SUBTIME(fehlzeit_bis, fehlzeit_von) 
        END))/(3600 * sollarbeitszeit.stunden_pro_tag), 2) AS 'VerfuegbarerUrlaub'
From fehlzeit
INNER JOIN mitarbeiter ON fehlzeit.mitarbeiter_id = mitarbeiter.mitarbeiter_id
INNER JOIN urlaub ON mitarbeiter.urlaub_id = urlaub.urlaub_id
INNER JOIN sollarbeitszeit ON mitarbeiter.sollarbeitszeit_id = sollarbeitszeit.sollarbeitszeit_id
WHERE fehlzeit.datum >= MAKEDATE(YEAR(CURRENT_DATE)-1, 1)
      AND DATE_ADD(MAKEDATE(YEAR(CURRENT_DATE), 1), INTERVAL -1 DAY) 
      AND fehlzeit.grund = 'Urlaub'
GROUP BY mitarbeiter.mitarbeiter_id
ORDER BY mitarbeiter.mitarbeiter_id ASC;
```

<br>

### Ausgabe SELECT-Statement
![ERD-Diagramm](./bilder/ausgabeSelect.png)

# Diskussion
Es wurde das erlernte Wissen, das im Zuge dieses Semesters und anhand des Oracle-Kurses angeeignet wurde, in Form dieses Semesterprojektes angewendet.
Für die WHERE-Bedingungen wäre die Verwendung unter SQL von Oracle Funktionen etwas einfacher und leserlicher, durch die bereitgestellte  Methode TRUNC(),
welche sehr einfach den Anfang und das Ende eines Jahres beziehungsweise Monats definieren lässt. Durch die Verwendung von MySql musste eine alternative Lösung gefunden und implementiert werden.

Trunc Oracle
<br>

# Literaturverzeichnis
* https://myacademy.oracle.com
* https://www.w3schools.com/sql
* https://github.com/FriendsOfREDAXO/markitup
* https://gitlab.com/hak-imst/stol/kolleg-education/kol-tinf/-/tree/master/Docker
* https://mvnrepository.com/artifact/mysql/mysql-connector-java/8.0.25
* https://www.nerdwest.de/downloads/db/mysqlJDBC.html

## Author
- Mathias Rudig
- Dario Skocibusic
