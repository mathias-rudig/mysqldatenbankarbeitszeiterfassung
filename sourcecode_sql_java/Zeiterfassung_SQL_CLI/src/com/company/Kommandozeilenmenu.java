package com.company;

import java.util.Scanner;

public class Kommandozeilenmenu {

    public static void start() {
        Scanner scan = new Scanner(System.in);
        String hauptmenu = "-";
        while (!hauptmenu.equals("x")) {
            Kommandozeilenmenu.ausgabeHauptmenueAnzeigen();
            System.out.print("> ");
            hauptmenu = scan.next();
            switch (hauptmenu) {
                case "1":
                    DbController.selectMitarbeiter();
                    zurueckHauptmenu();
                    break;
                case "2":
                    DbController.urlaubInformation();
                    zurueckHauptmenu();
                    break;
                case "3":
                    DbController.krankenstand();
                    zurueckHauptmenu();
                    break;
                case "4":
                    DbController.unentschuldigteFehlstunden();
                    zurueckHauptmenu();
                    break;
                case "5":
                    DbController.tageHomeofficeAktuellesJahr();
                    zurueckHauptmenu();
                    break;
                case "6":
                    DbController.arbeitszeitHomeoffice();
                    zurueckHauptmenu();
                    break;
                case "7":
                    DbController.arbeitszeitGesamt();
                    zurueckHauptmenu();
                    break;
                case "x":
                    break;
                default:
                    Kommandozeilenmenu.ausgabeFehlerEingabe();
                    break;
            }
        }


    }

    private static void ausgabeFehlerEingabe() {
        System.out.println("Falsche Eingabe!");
    }

    public static void ausgabeHauptmenueAnzeigen() {
        System.out.println("************************* MENU **************************");
        System.out.println("1) Alle Mitarbeiter ausgeben");
        System.out.println("2) Urlaubinformation aktuelles Jahr pro Mitarbeiter");
        System.out.println("3) Summe der Tage von Krankenstand pro Mitarbeiter");
        System.out.println("4) Summe der unentschuldigten Fehlstunden aktuelles Jahr pro Mitarbeiter");
        System.out.println("5) Summe der Tage von Homeoffice pro Mitarbeiter");
        System.out.println("6) Summe der Arbeitszeit von Homeoffice pro Mitarbeiter");
        System.out.println("7) Summe der Gesamt-Arbeitszeit pro Mitarbeiter");
        System.out.println("x) Beenden");
    }

    public static void zurueckHauptmenu() {
        Scanner scanEnter = new Scanner(System.in);
        String eingabe;
        System.out.println();
        do {
            System.out.println("Um ins Hauptmenü zu gelangen drücken Sie ENTER");
            eingabe = scanEnter.nextLine();
        } while (eingabe != "");
        eingabe = null;
    }
}
